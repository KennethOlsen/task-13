﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace Generics
{
    class Program
    {
        static void Main(string[] args)
        {

        
            List<Animal> animalList = new List<Animal>();

            // Creating a bunch of animals
            Animal a1 = new Animal() { Name = "Ole" };
            Animal a2 = new Animal() { Name = "Dole" };
            Animal a3 = new Animal() { Name = "Doffen" };
            Animal a4 = new Animal() { Name = "Donald" };
            Animal a5 = new Animal() { Name = "Mickey" };


            // inserting items at random places
            animalList.Insert(a1.randomInt(), a1);
            animalList.Insert(a2.randomInt(), a2);
            animalList.Insert(a3.randomInt(), a3);
            animalList.Insert(a4.randomInt(), a4);
            animalList.Insert(a5.randomInt(), a5);


            // Removing items at random places
            animalList.RemoveAt(a1.randomInt());
            animalList.RemoveAt(a3.randomInt());
            animalList.RemoveAt(a5.randomInt());

        }
    
    }
}

